package ssm.view;

import java.io.File;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.layout.Region;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import static ssm.StartupConstants.HTML_FILE_NAME;
import static ssm.file.SlideShowFileManager.SLASH;

/**
 * Class use to start webView and webEngine
 * @author Wenjun
 */
public class Browser extends Region{
    WebView browser = new WebView();
    WebEngine webEngine = browser.getEngine();
    
    public Browser(String add){
        File index = new File(add+ SLASH + HTML_FILE_NAME);
        webEngine.load(index.toURI().toString());
        getChildren().add(browser);
    }
        
    @Override protected void layoutChildren() {
        double w = getWidth();
        double h = getHeight();
        layoutInArea(browser,0,0,w,h,0, HPos.CENTER, VPos.CENTER);
    }

    @Override protected double computePrefWidth(double height) {
        return 750;
    }
 
    @Override protected double computePrefHeight(double width) {
        return 500;
    }

}
