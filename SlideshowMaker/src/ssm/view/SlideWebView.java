package ssm.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.geometry.Rectangle2D;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import static ssm.StartupConstants.CSS_FILE_NAME;
import static ssm.StartupConstants.HTML_FILE_NAME;
import static ssm.StartupConstants.JSON_FILE_NAME;
import static ssm.StartupConstants.JS_FILE_NAME;
import static ssm.StartupConstants.PATH_IMG;
import static ssm.StartupConstants.PATH_JAVASCRIPT;
import static ssm.StartupConstants.PATH_SITE;
import static ssm.StartupConstants.PATH_WEBCSS;
import static ssm.StartupConstants.PATH_WEB_PROP;
import static ssm.file.SlideShowFileManager.JSON_IMAGE_FILE_NAME;
import static ssm.file.SlideShowFileManager.JSON_SLIDES;
import static ssm.file.SlideShowFileManager.JSON_SLIDES_CAPTION;
import static ssm.file.SlideShowFileManager.JSON_TITLE;
import ssm.model.Slide;
import ssm.model.SlideShowModel;

/**
 * Class use to init web data and webView components.
 * @author Wenjun
 */
public class SlideWebView{
    Stage webStage = new Stage();
    Scene webScene;
    public static String SLASH = "/";
    SlideShowModel slideData;
    Slide currentSlide;
    SlideShowMakerView ui;
    
    File root, slideDir, js, css, img;
    
    public SlideWebView(SlideShowMakerView initUI){
        slideData = initUI.getSlideShow();
        root = new File(PATH_SITE);
        if(!root.exists()){
            root.mkdir();
        }
    }
    
    public void startWebView(){
       
        try {
            initDir();
            
            initPic();
            initCSS();
            initJS();
            initWebPage();
            
        } catch (IOException ex) {
            Logger.getLogger(SlideWebView.class.getName()).log(Level.SEVERE, null,ex);
        }
        
        try {
            if(initData()){
                webStage.setTitle(slideData.getTitle());
                webScene = new Scene(new Browser(slideDir.getPath()));
                
                Screen screen = Screen.getPrimary();
                Rectangle2D bounds = screen.getVisualBounds();
                webStage.setX(bounds.getMinX());
                webStage.setY(bounds.getMinY());
                webStage.setWidth(bounds.getWidth());
                webStage.setHeight(bounds.getHeight());
                
                webStage.setScene(webScene);
                webStage.show();
            }
        } catch (IOException ex) {
            Logger.getLogger(SlideWebView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void initDir(){
        slideDir = new File(PATH_SITE + SLASH + slideData.getTitle());
        js = new File(slideDir.getPath() + SLASH + PATH_JAVASCRIPT);
        css = new File(slideDir.getPath() + SLASH + PATH_WEBCSS);
        img = new File(slideDir.getPath() + SLASH + PATH_IMG);

        if(slideDir.exists()){
            File[] entries = slideDir.listFiles();
            for(File f : entries){
                if(f.isDirectory()){
                    File[] data = f.listFiles();
                    for(File i : data){
                        i.delete();
                    }
                }
                f.delete();
            }
            slideDir.delete();
                
        }
            slideDir.mkdir();
            js.mkdir();
            css.mkdir();
            img.mkdir();
            
    }
    
    public void initPic() throws IOException{
        for(Slide slide : slideData.getSlides()){
            File temp = new File(slide.getImagePath() + SLASH +slide.getImageFileName());
            File pic = new File(img.getPath() + SLASH + slide.getImageFileName());
            if(!pic.exists()){
                Files.copy(temp.toPath(), pic.toPath());
            }
        }
    }
    
    public boolean initData() throws FileNotFoundException, IOException{
        OutputStream os = new FileOutputStream(js.getPath() +SLASH+ JSON_FILE_NAME);
        JsonWriter jsonWriter = Json.createWriter(os);
        
        JsonArray slidesJsonArray = makeJsonArray();
        JsonObject slidesJsonObject = Json.createObjectBuilder()
                .add(JSON_TITLE, slideData.getTitle())
                .add(JSON_SLIDES, slidesJsonArray)
                .build();
        jsonWriter.writeObject(slidesJsonObject);
        return true;
    }
   
    
    public void initCSS() throws IOException{
        File temp = new File(PATH_WEB_PROP + "sample.css");
        File cssFile = new File(css.getPath() + SLASH + CSS_FILE_NAME);
        Files.copy(temp.toPath(), cssFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }
    
    public void initJS() throws IOException{
        File temp = new File(PATH_WEB_PROP + "sample.js");
        File jsFile = new File(js.getPath() + SLASH + JS_FILE_NAME);
        Files.copy(temp.toPath(), jsFile.toPath(), StandardCopyOption.REPLACE_EXISTING);    
    }
    
    public void initWebPage() throws IOException{
        File temp = new File(PATH_WEB_PROP + "sample.html");
        File web = new File(slideDir.getPath() + SLASH + HTML_FILE_NAME);
        Files.copy(temp.toPath(), web.toPath(), StandardCopyOption.REPLACE_EXISTING);    

    }
    
    private JsonArray makeJsonArray() {
        int i;
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        
        for(i = 0; i <slideData.getSlides().size(); i++){
            JsonObject jso = makeJsonObject(slideData.getSlides().get(i).getImageFileName(), slideData.getSlides().get(i).getCaption());
            jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    private JsonObject makeJsonObject(String name, String caption) {
       JsonObject jso = Json.createObjectBuilder()
               .add(JSON_IMAGE_FILE_NAME, name)
               .add(JSON_SLIDES_CAPTION, caption)
               .build();
       return jso;
       
    }
}
