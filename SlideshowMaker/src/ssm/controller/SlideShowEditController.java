package ssm.controller;

import java.util.Optional;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.MouseEvent;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.DEFAULT_IMAGE_CAPTION;
import static ssm.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static ssm.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import ssm.model.Slide;
import ssm.model.SlideShowModel;
import ssm.view.SlideShowMakerView;

/**
 * This controller provides responses for the slideshow edit toolbar,
 * which allows the user to add, remove, and reorder slides.
 * 
 * @author McKilla Gorilla & Wenjun Qu
 */
public class SlideShowEditController {
    // APP UI
    private SlideShowMakerView ui;
    
    /**
     * This constructor keeps the UI for later.
     */
    public SlideShowEditController(SlideShowMakerView initUI) {
	ui = initUI;
    }
    
    /**
     * Provides a response for when the user wishes to add a new
     * slide to the slide show.
     */
    public void processAddSlideRequest() {
	SlideShowModel slideShow = ui.getSlideShow();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	slideShow.addSlide(DEFAULT_SLIDE_IMAGE, PATH_SLIDE_SHOW_IMAGES, null);
    }
    public void processRemoveSlideRequest(){
        SlideShowModel slideShowModel = ui.getSlideShow();
        slideShowModel.removeSelectedSlide();
        
    }
    public void processMoveupSlideRequest(){
        SlideShowModel slideShowModel=ui.getSlideShow();
        slideShowModel.moveupSelectedSlide();
    }
    public void processMovedownSlideRequest(){
        SlideShowModel slideShowModel=ui.getSlideShow();
        slideShowModel.movedownSelectedSlide();
    }
    public void processPreviousSlideRequest(){
        
    }
    public void processNextSlideRequest(){
        
    }
    
    public void processSelectedSlide(SlideShowModel slideShow, Slide slide, EventHandler<MouseEvent> aThis) {
           slideShow.setSelectedSlide(slide);
           ui.updateSlideEditBarControls(slideShow);
        ui.reloadSlideShowPane(slideShow);

    }
    public void processAddTitleRequest(){
        SlideShowModel slideShow = ui.getSlideShow();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	//String title = JOptionPane.showInputDialog(null, "Enter title: ", "Title", 1);
        //slideShow.setTitle(title);
        TextInputDialog title = new TextInputDialog();
        title.setTitle("");
        title.setHeaderText("");
        title.setContentText("Please enter the Slide title");
        
        Optional<String> result = title.showAndWait();
        if(result.isPresent()){
            slideShow.setTitle(result.get());
        }
    }


}
