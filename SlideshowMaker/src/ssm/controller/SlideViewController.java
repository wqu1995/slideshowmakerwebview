
package ssm.controller;

import ssm.model.Slide;
import ssm.model.SlideShowModel;
import ssm.view.SlideView;

/**
 * This controller provides responses for the slideView toolbar,
 * which allows the user to load next and previous slide thats ready to show.
 * @author Wenjun
 */
public class SlideViewController {
    private SlideView ui;
    


    public SlideViewController(SlideView initUI) {
        ui = initUI;
    }
    
    public void processPreviousRequest(){
        SlideShowModel slides = ui.getViewSlides();
        Slide selectedSlide = ui.getCurrentSlide();
        int i = 0;
        while(selectedSlide != slides.getSlides().get(i)){
            i++;
        }
        if(selectedSlide == slides.getSlides().get(0)){
            ui.setCurrentSlide(slides.getSlides().get(slides.getSlides().size()-1));
        }
        else{
            ui.setCurrentSlide(slides.getSlides().get(i-1));
        }
        ui.displaySlide();
    }
    
    public void processNextRequest(){
        SlideShowModel slides = ui.getViewSlides();
        Slide selectedSlide = ui.getCurrentSlide();
        int i = 0;
        while(selectedSlide != slides.getSlides().get(i)){
            i++;
        }
        if(selectedSlide == slides.getSlides().get(slides.getSlides().size()-1)){
            ui.setCurrentSlide(slides.getSlides().get(0));
        }
        else{
            ui.setCurrentSlide(slides.getSlides().get(i+1));
        }
        ui.displaySlide();
    }
}
