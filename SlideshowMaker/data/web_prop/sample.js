	var slideData;
	var i = 0;
	var Bplay = 0;
	var myVar;
	var reload = 0;

	function nextImage(){
		if(i == slideData.slides.length-1){
			i = 0;
		}
		else{
			i++;
		}
		imagePath = "test/" + slideData.slides[i].image_file_name;

		$("#myImage").fadeOut(function(){
			$("#myImage").attr('src', "img/" + slideData.slides[i].image_file_name);
		}).fadeIn();
			if(slideData.slides[i].caption == " "){
				document.getElementById("myCaption").innerHTML = "&nbsp";
			
			}
			else{
				$("#myCaption").fadeOut( function() {
	  				$(this).text(slideData.slides[i].caption);
				}).fadeIn();

			}
	}
	function previousImage(){
		if(i == 0){
			i = (slideData.slides.length)-1;
		}
		else{
			i--;
		}
		$("#myImage").fadeOut(function(){
			$("#myImage").attr('src',"img/" + slideData.slides[i].image_file_name);
		}).fadeIn();
			if(slideData.slides[i].caption == " "){
				document.getElementById("myCaption").innerHTML = "&nbsp";
			
			}
			else{
				$("#myCaption").fadeOut( function() {
	  				$(this).text(slideData.slides[i].caption);
				}).fadeIn();

			}
	}
	function playImage(){
		if(Bplay ==0){
			myVar = setInterval(nextImage, 3000);
			Bplay = 1;
		}
		else if(Bplay ==1){
			clearInterval(myVar);
			Bplay = 0;
		}
	}


	$(document).ready(function(){
		$.getJSON('js/webData.json', function(data){
		slideData = data;
		document.getElementById("myTitle").innerHTML = (slideData.title.toString());
		document.getElementById("myImage").src = "img/"+slideData.slides[0].image_file_name;
		if(slideData.slides[0].caption == " "){
			document.getElementById("myCaption").innerHTML = "&nbsp";

		}
		else{
			document.getElementById("myCaption").innerHTML = slideData.slides[0].caption;

		}
		});
		window.onload = function() {
    		if(!window.location.hash) {
        	window.location = window.location + '#loaded';
        	window.location.reload();
    	}
	}


		$("#playButton").click(
			function(){
			$(this).find('span').toggleClass('glyphicon-play').toggleClass('glyphicon-pause');
			playImage();
		});

		$("#nextButton").click(function(){
			nextImage();	
		});

		$("#previousButton").click(function(){
			previousImage();
		});

	});
